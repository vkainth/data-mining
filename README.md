# data-mining
Repository for CSE 5334

## Running the IPython file on a local machine
Install python version 3.5 and pip using recommended methods for your operating system.

Then run the following command: `pip install -r requirements.txt` to install all the requirements on your computer.
Simply run `jupyter notebook` to open the IPython Notebook and access the file by clicking on `DataMining1.ipynb`

## Running the IPython file online
Go to [NBViewer](https://nbviewer.jupyter.org/) and paste the URL of this repository in the search bar. Hit enter and select
the file `DataMining1.ipynb`
